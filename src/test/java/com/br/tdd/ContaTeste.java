package com.br.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ContaTeste {

    private Cliente cliente;
    private Conta conta;

    @BeforeEach
    public void setUp(){
        this.cliente = new Cliente("Anderson");
        this.conta =  new Conta(this.cliente, 200.00);
    }

    @Test
    public void testarDepositoEmConta(){
        double valodeDeDeposito = 400.00;
        this.conta.depositar(valodeDeDeposito);

        Assertions.assertEquals(420.00, this.conta.getSaldo());
    }

    @Test
    public void testarSacarDeConta(){
        double valodeDeSaque = 50.00;
        conta.sacar(valodeDeSaque);

        Assertions.assertEquals(50.00, conta.getSaldo());
    }

    @Test
    public void testarSaqueSemSaldoNaConta(){
        double valodeDeSaque = 400.00;

        Assertions.assertThrows(RuntimeException.class,/* lambda */() -> {conta.sacar(valodeDeSaque);});
    }
}
