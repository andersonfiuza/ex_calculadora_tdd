package com.br.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculadoraTeste {

    @Test
    public void testarSomaDeDoisNumero(){
        int resultado = Calculadora.soma(2, 2);
        Assertions.assertEquals(4, resultado);
    }
    @Test
    public void divisaoDoisNumerosflutuantes(){
        double resultado = Calculadora.divisao(5.8, 9.5);
        Assertions.assertEquals(0.61, resultado);
    }
    @Test
    public void divisaoDoisNumerosInteiros(){
        int resultado = Calculadora.divisao(4, 2);
        Assertions.assertEquals(2, resultado);
    }

    @Test
    public void multiplicacaoNumerosFlutuantes(){
        double resultado = Calculadora.multiplicacao(4.5, 2.4);
        Assertions.assertEquals(10.8, resultado);
    }

    @Test
    public void multiplicacaoNumerosInteiros(){
        int resultado = Calculadora.multiplicacao(4, 4);
        Assertions.assertEquals(16, resultado);
    }






}
