package com.br.tdd;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Calculadora {

    public static int soma(int primeiroNumero, int segundoNumero){
        int resultado = primeiroNumero + segundoNumero;
        return resultado;
    }

    public static int divisao(int primeiroNumero, int segundoNumero){
        int resultado = primeiroNumero / segundoNumero;
        return resultado;
    }


    public static double divisao(double primeiroNumero, double  segundoNumero){
        double resultado = primeiroNumero / segundoNumero;
        BigDecimal valorArrendododado = new BigDecimal(resultado).setScale(2,RoundingMode.DOWN);

        return valorArrendododado.doubleValue();
    }

    public static double multiplicacao(double primeiroNumero, double segundoNumero){

        double  resultado = primeiroNumero * segundoNumero;
        BigDecimal valorArrendododado = new BigDecimal(resultado).setScale(1, RoundingMode.DOWN);

        return valorArrendododado.doubleValue();
    }

    public static int multiplicacao(int primeiroNumero, int segundoNumero){
        int resultado = primeiroNumero * segundoNumero;
        return resultado;
    }

}
